﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AudioRawdataParsingTool
{
    class Utils
    {
        public static void BindDataTable(System.Data.DataTable dt, List<string> RawdataList, string collumnName)
        {
            System.Data.DataRow row;
            if (!dt.Columns.Contains(collumnName))
            {
                dt.Columns.Add(collumnName, System.Type.GetType("System.String"));
                for (int i = 0; i < RawdataList.Count; i++)
                {
                    row = dt.NewRow();
                    row[collumnName] = RawdataList[i];
                    dt.Rows.Add(row);

                }
            }
        }

        public static string get_Base_Directory()
        {
            return AppDomain.CurrentDomain.BaseDirectory;
        }
    }
}
