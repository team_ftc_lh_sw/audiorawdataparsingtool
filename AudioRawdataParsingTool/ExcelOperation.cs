﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Excel = Microsoft.Office.Interop.Excel;
using System.IO;
using System.Windows.Forms;
using System.Data;
using Microsoft.Office.Interop.Excel;
using System.Diagnostics;


namespace AudioRawdataParsingTool
{
    class ExcelOperation
    {
        public Microsoft.Office.Interop.Excel.Application excelApp;
        object Nothing = System.Reflection.Missing.Value;


        public ExcelOperation()
        {
            excelApp = new Microsoft.Office.Interop.Excel.Application();
        }

        public void copy_Excel(string SourceFile_Directory, string OutputFile_Directory)
        {
            if (File.Exists(SourceFile_Directory))
            {
                if (File.Exists(OutputFile_Directory))
                {
                    File.Delete(OutputFile_Directory);
                }
                File.Copy(SourceFile_Directory, OutputFile_Directory);
            }
            else
            {
                MessageBox.Show("在该路径下 " + Utils.get_Base_Directory() + " 未找到 Template.xlsx 模板文件！请先将文件放置此处!");
            }
        }

        public void batch_Create_Excel(string[] arrayExcelName, string dir)
        {
            string ExcelFilePath = "";
            Microsoft.Office.Interop.Excel.Workbook wb = null;

            if (arrayExcelName != null)
            {
                for (int i = 0; i < arrayExcelName.Length; i++)
                {
                    ExcelFilePath = dir + "\\" + arrayExcelName[i] + Constant.EXCEL_FILE_SUFFIX_XLS;
                    if (File.Exists(ExcelFilePath))
                    {
                        File.Delete(ExcelFilePath);
                    }
                    wb = excelApp.Workbooks.Add(Nothing);
                    wb.Sheets[2].Delete();
                    wb.Sheets[2].Delete();
                    wb.SaveAs(ExcelFilePath, System.Type.Missing, System.Type.Missing, System.Type.Missing, System.Type.Missing, System.Type.Missing, Microsoft.Office.Interop.Excel.XlSaveAsAccessMode.xlNoChange, System.Type.Missing, System.Type.Missing, System.Type.Missing, System.Type.Missing, System.Type.Missing);
                    wb.Close();
                    NAR(wb);
                }
            }
        }

        public void get_FX100_Rawdata_By_Workbook(System.Data.DataTable dt, string FilePath)
        {
            List<string> Rawdata = new List<string>();
            int StartColumn = 0, EndColumn = 0;
            List<int> Location = new List<int>();
            Location.Add(8);

            Microsoft.Office.Interop.Excel.Workbook wb_RawdataFile = excelApp.Workbooks.Open(FilePath,
                              Nothing, Nothing, Nothing, Nothing, Nothing, Nothing, Nothing,
                              Nothing, Nothing, Nothing, Nothing, Nothing, Nothing, Nothing);

            int sheetCount = wb_RawdataFile.Sheets.Count;

            for (int i = 1; i < sheetCount + 1; i++)
            {
                Rawdata.Clear();
                Microsoft.Office.Interop.Excel.Worksheet ws_RawdataFile = wb_RawdataFile.Sheets[i];
                string sheetName = ws_RawdataFile.Name;
                StartColumn = Location[0];
                if (sheetName.Equals(Constant.TEMPLATE_SHEET_FR))
                {
                    EndColumn = 100;
                }
                else if (sheetName.Equals(Constant.TEMPLATE_SHEET_THD))
                {
                    EndColumn = 88;
                }
                else if (sheetName.Equals(Constant.TEMPLATE_SHEET_RB))
                {
                    EndColumn = 60;
                }

                for (int j = StartColumn; j < EndColumn + 1; j++)
                {
                    string Rawdata_value = ((Range)ws_RawdataFile.Cells[6, j]).Text.ToString();
                    Rawdata.Add(Rawdata_value);
                }

                Utils.BindDataTable(dt, Rawdata, sheetName);
            }

            wb_RawdataFile.Close();
            NAR(wb_RawdataFile);
        }

        public List<string> get_UPV_Rawdata_By_KeyWords(string FilePath, string FindWhat)
        {
            List<string> Rawdata = new List<string>();
            int StartRow = 0, EndRow = 0;
            string Rawdata_value = "";

            List<string> Location = find_FirstandLast_TextLocation(FilePath, FindWhat);
            if (Location.Count <= 0)
            {
                return Rawdata;
            }

            Microsoft.Office.Interop.Excel.Workbook wb_RawdataFile = excelApp.Workbooks.Open(FilePath,
                              Nothing, Nothing, Nothing, Nothing, Nothing, Nothing, Nothing,
                              Nothing, Nothing, Nothing, Nothing, Nothing, Nothing, Nothing);

            Microsoft.Office.Interop.Excel.Worksheet ws_RawdataFile = wb_RawdataFile.Sheets[1];
            StartRow = Convert.ToInt32(Location[0]);

            if (Location.Count == 1)
            {
                EndRow = StartRow;
            }
            else
            {
                EndRow = Convert.ToInt32(Location[1]);
            }

            for (int i = StartRow; i < EndRow + 1; i++)
            {
                Rawdata_value = ((Range)ws_RawdataFile.Cells[i, Constant.RAWDATA_COLLUMN]).Text.ToString();
                Rawdata.Add(Rawdata_value);
            }

            wb_RawdataFile.Close();
            NAR(wb_RawdataFile);

            return Rawdata;
        }

        public List<string> find_FirstandLast_TextLocation(string FilePath, string FindWhat)
        {
            Excel.Range rgUsed = null, rgFound = null, rgTmp = null, rgTmpLast = null;
            int StartRow = 0;
            int LastRow = 0;
            int tempRow = 0;
            string item = "";
            List<string> Location = new List<string>();

            Microsoft.Office.Interop.Excel.Workbook wb = excelApp.Workbooks.Open(FilePath,
                                   Nothing, Nothing, Nothing, Nothing, Nothing, Nothing, Nothing,
                                   Nothing, Nothing, Nothing, Nothing, Nothing, Nothing, Nothing);


            Microsoft.Office.Interop.Excel.Worksheet ws = wb.Sheets[1];

            rgUsed = ws.get_Range("A2:K313");
            rgFound = (Excel.Range)rgUsed.Find(FindWhat, Type.Missing,
                Excel.XlFindLookIn.xlValues,//查找值，或者xlFormulas查找公式等
                Excel.XlLookAt.xlPart, //这里用xlWhole返回的一直是空指针
                Excel.XlSearchOrder.xlByRows,//按行查找
                Excel.XlSearchDirection.xlNext, //建议就用xlNext
                false, false);
            if (rgFound == null)
            {
                wb.Close(false);
                NAR(wb);
                return Location;
            }

            rgTmp = rgFound;

            while (rgTmp != null)
            {
                item = rgTmp.Value2;
                if (item.Equals(FindWhat))
                {
                    StartRow = rgTmp.Row;
                    Location.Add(Convert.ToString(StartRow));
                    break;
                }
            }

            while (rgTmp != null)
            {
                rgTmp = rgUsed.FindNext(rgTmp);
                item = rgTmp.Value2;
                tempRow = rgTmp.Row;
                if (!item.Equals(FindWhat) || tempRow == StartRow)
                {
                    if (!item.Equals(""))
                    {
                        break;
                    }
                }
            }

            rgTmpLast = rgUsed.FindPrevious(rgTmp);
            LastRow = rgTmpLast.Row;
            if (LastRow != StartRow)
            {
                Location.Add(Convert.ToString(LastRow));
            }

            wb.Close(false);
            NAR(wb);

            return Location;
        }

        public Boolean export_DataTable_To_Excel(string Output_FilePath, List<System.Data.DataTable> dtAllRawdata,
            Dictionary<string, List<int>> dic_samplecode_position, bool need_copy_template_file)
        {
            Boolean isSuccess = false;
            if (need_copy_template_file)
            {
                copy_Excel(Utils.get_Base_Directory() + Constant.TEMPLATE_FILENAME, Output_FilePath);
            }


            if (File.Exists(Output_FilePath))
            {
                Microsoft.Office.Interop.Excel.Workbook OutputWorkBook = excelApp.Workbooks.Open(Output_FilePath,
                                   Nothing, Nothing, Nothing, Nothing, Nothing, Nothing, Nothing,
                                   Nothing, Nothing, Nothing, Nothing, Nothing, Nothing, Nothing);

                for (int i = 0; i < dtAllRawdata.Count; i++)
                {
                    switch (dtAllRawdata[i].TableName)
                    {
                        case "dt_RD":
                            write_Data_By_SheetName(OutputWorkBook, dtAllRawdata[i], Constant.TEMPLATE_SHEET_RD, dic_samplecode_position);
                            break;

                        case "dt_SD":
                            write_Data_By_SheetName(OutputWorkBook, dtAllRawdata[i], Constant.TEMPLATE_SHEET_SD, dic_samplecode_position);
                            break;

                        case "dt_RLR":
                            write_Data_By_SheetName(OutputWorkBook, dtAllRawdata[i], Constant.TEMPLATE_SHEET_RLR, dic_samplecode_position);
                            break;

                        case "dt_SLR":
                            write_Data_By_SheetName(OutputWorkBook, dtAllRawdata[i], Constant.TEMPLATE_SHEET_SLR, dic_samplecode_position);
                            break;

                        case "dt_Speaker FR":
                            write_Data_By_SheetName(OutputWorkBook, dtAllRawdata[i], Constant.TEMPLATE_SHEET_FR, dic_samplecode_position);
                            break;

                        case "dt_Speaker THD":
                            write_Data_By_SheetName(OutputWorkBook, dtAllRawdata[i], Constant.TEMPLATE_SHEET_THD, dic_samplecode_position);
                            break;

                        case "dt_Speaker R&B":
                            write_Data_By_SheetName(OutputWorkBook, dtAllRawdata[i], Constant.TEMPLATE_SHEET_RB, dic_samplecode_position);
                            break;

                        default:
                            break;
                    }
                }
                OutputWorkBook.Save();
                OutputWorkBook.Close();
                NAR(OutputWorkBook);
                isSuccess = true;
            }
            return isSuccess;
        }

        private void write_Data_By_SheetName(Microsoft.Office.Interop.Excel.Workbook wb, System.Data.DataTable dt, string sheetName, Dictionary<string, List<int>> dic_samplecode_position)
        {
            string[] Array_ColumnName = null, TableName = null;
            string ColumnName = "", SampleCode = "", Row_Text = "";
            int SampleCodeLocation_Row = 0, SampleCodeLocation_Column = 0;
            int Rawdata_Start_Row = 0, Rawdata_Start_Column = 0;
            int Columnindex = 0;

            if (dt == null)
            {
                return;
            }

            TableName = dt.TableName.Split('_');
            if (TableName.Length != 2)
            {
                return;
            }

            Microsoft.Office.Interop.Excel.Worksheet OutputWorkSheet = wb.Sheets[sheetName];

            for (int i = 0; i < dt.Columns.Count; i++)
            {
                ColumnName = dt.Columns[i].ColumnName;
                Array_ColumnName = ColumnName.Split('_');

                if (Array_ColumnName.Length == 2)
                {
                    SampleCode = Array_ColumnName[1];

                    // get samplecode location
                    SampleCodeLocation_Row = dic_samplecode_position[SampleCode][0];
                    SampleCodeLocation_Column = dic_samplecode_position[SampleCode][1];
                    Rawdata_Start_Row = 22;

                    if (Array_ColumnName[0].Equals(Constant.BEFORE)
                        || Array_ColumnName[0].Equals(Constant.TEMP_FOLDER_BEFORE))
                    {
                        Rawdata_Start_Column = SampleCodeLocation_Column + 1;
                    }
                    else
                    {
                        if (sheetName.Equals(Constant.TEMPLATE_SHEET_SLR)
                            || sheetName.Equals(Constant.TEMPLATE_SHEET_RLR)
                            || sheetName.Equals(Constant.TEMPLATE_SHEET_FR))
                        {
                            Rawdata_Start_Column = SampleCodeLocation_Column + 4;
                        }
                        else
                        {
                            Rawdata_Start_Column = SampleCodeLocation_Column + 3;
                        }
                    }


                    // Write data
                    OutputWorkSheet.Cells[SampleCodeLocation_Row, SampleCodeLocation_Column] = SampleCode;
                    Columnindex = dt.Columns.IndexOf(ColumnName);
                    int z = 0;
                    for (int j = 0; j < dt.Rows.Count; j++)
                    {
                        Row_Text = dt.Rows[j].ItemArray[Columnindex].ToString();
                        if (!Row_Text.Equals(""))
                        {
                            OutputWorkSheet.Cells[Rawdata_Start_Row + z, Rawdata_Start_Column] = Row_Text;
                            z++;
                        }
                    }
                }
            }
        }

        public void parsing_TXT2Excel(FileInfo[] subTxtFileInfo, string tempFolder, string excelFilePath)
        {
            if (subTxtFileInfo != null && File.Exists(excelFilePath))
            {
                for (int i = 0; i < subTxtFileInfo.Length; i++)
                {
                    if (!subTxtFileInfo[i].Name.Contains(Constant.TXT_NAME_PASSEDFAILED))
                    {
                        copy_TXTData2Excel(subTxtFileInfo[i].FullName, tempFolder, excelFilePath);
                    }
                }
            }

        }

        public void copy_TXTData2Excel(string txtFilePath, string tempFolder, string excelFilePath)
        {
            Microsoft.Office.Interop.Excel.Workbook temptxtwb = null;
            Microsoft.Office.Interop.Excel.Workbook wb_Rawdata = null;
            Microsoft.Office.Interop.Excel.Worksheet tempsheet = null;
            Microsoft.Office.Interop.Excel.Worksheet CopiedSheet = null;

            string copiedFilePath = tempFolder + Constant.TEMP_EXCELFILE;
            string CellValue = "";

            if (!File.Exists(copiedFilePath))
            {
                File.Copy(excelFilePath, copiedFilePath);
            }


            //parsing data to temp excel
            temptxtwb = excelApp.Workbooks.Open(txtFilePath,
                              Nothing, Nothing, Nothing, Nothing, Nothing, Nothing, Nothing,
                              Nothing, Nothing, Nothing, Nothing, Nothing, Nothing, Nothing);
            tempsheet = temptxtwb.Sheets[1];
            temptxtwb.SaveCopyAs(copiedFilePath);



            //copy temp excel data to excel
            wb_Rawdata = excelApp.Workbooks.Open(excelFilePath,
                              Nothing, Nothing, Nothing, Nothing, Nothing, Nothing, Nothing,
                              Nothing, Nothing, Nothing, Nothing, Nothing, Nothing, Nothing);
            tempsheet.Copy(wb_Rawdata.Sheets[1], Type.Missing);
            CopiedSheet = wb_Rawdata.Sheets[1];
            CellValue = ((Range)CopiedSheet.Cells[2, 4]).Text.ToString();

            if (CellValue.Contains("FR"))
            {
                CopiedSheet.Name = Constant.TEMPLATE_SHEET_FR;
            }
            else if (CellValue.Contains("R&B"))
            {
                CopiedSheet.Name = Constant.TEMPLATE_SHEET_RB;
            }
            else if (CellValue.Contains("THD"))
            {
                CopiedSheet.Name = Constant.TEMPLATE_SHEET_THD;
            }
            if (wb_Rawdata.Sheets.Count > 3)
            {
                wb_Rawdata.Sheets[4].Delete();
            }

            wb_Rawdata.Save();
            wb_Rawdata.Close();
            temptxtwb.Close();
            NAR(wb_Rawdata);
            NAR(temptxtwb);

        }

        public void quitExcelApp()
        {
            if (excelApp != null)
            {
                excelApp.Quit();
                NAR(excelApp);
            }
        }

        public void NAR(Object o)
        {
            // Reference https://support.microsoft.com/zh-tw/kb/317109
            try
            {
                //if (o != null)
                //    System.Runtime.InteropServices.Marshal.FinalReleaseComObject(o);
                while (System.Runtime.InteropServices.Marshal.ReleaseComObject(o) > 0) ;
            }
            finally { o = null; }
        }
    }
}
