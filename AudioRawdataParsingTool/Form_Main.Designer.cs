﻿namespace AudioRawdataParsingTool
{
    partial class Form_Main
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox_SelectSourceFilePath = new System.Windows.Forms.GroupBox();
            this.button_SelectFX100SourceFilePath = new System.Windows.Forms.Button();
            this.textBox_FX100SourceFilePath = new System.Windows.Forms.TextBox();
            this.button_SelectUPVItemSourceFilePath = new System.Windows.Forms.Button();
            this.textBox_UPVItemSourceFilePath = new System.Windows.Forms.TextBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.button_GenerateMergeFile = new System.Windows.Forms.Button();
            this.button_SelectOuputFilePath = new System.Windows.Forms.Button();
            this.textBox_SelectOutputFilePath = new System.Windows.Forms.TextBox();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.帮助ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.groupBox_SelectSourceFilePath.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox_SelectSourceFilePath
            // 
            this.groupBox_SelectSourceFilePath.Controls.Add(this.button_SelectFX100SourceFilePath);
            this.groupBox_SelectSourceFilePath.Controls.Add(this.textBox_FX100SourceFilePath);
            this.groupBox_SelectSourceFilePath.Controls.Add(this.button_SelectUPVItemSourceFilePath);
            this.groupBox_SelectSourceFilePath.Controls.Add(this.textBox_UPVItemSourceFilePath);
            this.groupBox_SelectSourceFilePath.Location = new System.Drawing.Point(12, 40);
            this.groupBox_SelectSourceFilePath.Name = "groupBox_SelectSourceFilePath";
            this.groupBox_SelectSourceFilePath.Size = new System.Drawing.Size(573, 84);
            this.groupBox_SelectSourceFilePath.TabIndex = 0;
            this.groupBox_SelectSourceFilePath.TabStop = false;
            this.groupBox_SelectSourceFilePath.Text = "1. UPVItem and FX100 原始数据档案夹路径";
            // 
            // button_SelectFX100SourceFilePath
            // 
            this.button_SelectFX100SourceFilePath.Location = new System.Drawing.Point(428, 48);
            this.button_SelectFX100SourceFilePath.Name = "button_SelectFX100SourceFilePath";
            this.button_SelectFX100SourceFilePath.Size = new System.Drawing.Size(139, 23);
            this.button_SelectFX100SourceFilePath.TabIndex = 3;
            this.button_SelectFX100SourceFilePath.Text = "FX100 原始数据";
            this.button_SelectFX100SourceFilePath.UseVisualStyleBackColor = true;
            this.button_SelectFX100SourceFilePath.Click += new System.EventHandler(this.button_SelectFX100SourceFilePath_Click);
            // 
            // textBox_FX100SourceFilePath
            // 
            this.textBox_FX100SourceFilePath.Location = new System.Drawing.Point(8, 50);
            this.textBox_FX100SourceFilePath.Name = "textBox_FX100SourceFilePath";
            this.textBox_FX100SourceFilePath.Size = new System.Drawing.Size(414, 22);
            this.textBox_FX100SourceFilePath.TabIndex = 2;
            // 
            // button_SelectUPVItemSourceFilePath
            // 
            this.button_SelectUPVItemSourceFilePath.Location = new System.Drawing.Point(428, 21);
            this.button_SelectUPVItemSourceFilePath.Name = "button_SelectUPVItemSourceFilePath";
            this.button_SelectUPVItemSourceFilePath.Size = new System.Drawing.Size(139, 23);
            this.button_SelectUPVItemSourceFilePath.TabIndex = 1;
            this.button_SelectUPVItemSourceFilePath.Text = "UPVItem 原始数据";
            this.button_SelectUPVItemSourceFilePath.UseVisualStyleBackColor = true;
            this.button_SelectUPVItemSourceFilePath.Click += new System.EventHandler(this.button_SelectUPVItemSourceFilePath_Click);
            // 
            // textBox_UPVItemSourceFilePath
            // 
            this.textBox_UPVItemSourceFilePath.Location = new System.Drawing.Point(8, 23);
            this.textBox_UPVItemSourceFilePath.Name = "textBox_UPVItemSourceFilePath";
            this.textBox_UPVItemSourceFilePath.Size = new System.Drawing.Size(414, 22);
            this.textBox_UPVItemSourceFilePath.TabIndex = 0;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.button_GenerateMergeFile);
            this.groupBox2.Controls.Add(this.button_SelectOuputFilePath);
            this.groupBox2.Controls.Add(this.textBox_SelectOutputFilePath);
            this.groupBox2.Location = new System.Drawing.Point(12, 154);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(573, 116);
            this.groupBox2.TabIndex = 1;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "2. 结果整合档案输出档案夹路径";
            // 
            // button_GenerateMergeFile
            // 
            this.button_GenerateMergeFile.Location = new System.Drawing.Point(47, 68);
            this.button_GenerateMergeFile.Name = "button_GenerateMergeFile";
            this.button_GenerateMergeFile.Size = new System.Drawing.Size(465, 33);
            this.button_GenerateMergeFile.TabIndex = 5;
            this.button_GenerateMergeFile.Text = "产生结果整合档案";
            this.button_GenerateMergeFile.UseVisualStyleBackColor = true;
            this.button_GenerateMergeFile.Click += new System.EventHandler(this.button_GenerateMergeFile_Click);
            // 
            // button_SelectOuputFilePath
            // 
            this.button_SelectOuputFilePath.Location = new System.Drawing.Point(428, 28);
            this.button_SelectOuputFilePath.Name = "button_SelectOuputFilePath";
            this.button_SelectOuputFilePath.Size = new System.Drawing.Size(139, 23);
            this.button_SelectOuputFilePath.TabIndex = 4;
            this.button_SelectOuputFilePath.Text = "选择档案夹路径";
            this.button_SelectOuputFilePath.UseVisualStyleBackColor = true;
            this.button_SelectOuputFilePath.Click += new System.EventHandler(this.button_SelectOuputFilePath_Click);
            // 
            // textBox_SelectOutputFilePath
            // 
            this.textBox_SelectOutputFilePath.Location = new System.Drawing.Point(8, 30);
            this.textBox_SelectOutputFilePath.Name = "textBox_SelectOutputFilePath";
            this.textBox_SelectOutputFilePath.Size = new System.Drawing.Size(414, 22);
            this.textBox_SelectOutputFilePath.TabIndex = 3;
            // 
            // menuStrip1
            // 
            this.menuStrip1.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.帮助ToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(598, 24);
            this.menuStrip1.TabIndex = 3;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // 帮助ToolStripMenuItem
            // 
            this.帮助ToolStripMenuItem.Name = "帮助ToolStripMenuItem";
            this.帮助ToolStripMenuItem.Size = new System.Drawing.Size(44, 20);
            this.帮助ToolStripMenuItem.Text = "帮助";
            this.帮助ToolStripMenuItem.Click += new System.EventHandler(this.帮助ToolStripMenuItem_Click);
            // 
            // Form_Main
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(598, 276);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox_SelectSourceFilePath);
            this.Controls.Add(this.menuStrip1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "Form_Main";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "AudioRawdataParsingTool v1.0.1";
            this.groupBox_SelectSourceFilePath.ResumeLayout(false);
            this.groupBox_SelectSourceFilePath.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox_SelectSourceFilePath;
        private System.Windows.Forms.Button button_SelectFX100SourceFilePath;
        private System.Windows.Forms.TextBox textBox_FX100SourceFilePath;
        private System.Windows.Forms.Button button_SelectUPVItemSourceFilePath;
        private System.Windows.Forms.TextBox textBox_UPVItemSourceFilePath;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Button button_GenerateMergeFile;
        private System.Windows.Forms.Button button_SelectOuputFilePath;
        private System.Windows.Forms.TextBox textBox_SelectOutputFilePath;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem 帮助ToolStripMenuItem;

    }
}

