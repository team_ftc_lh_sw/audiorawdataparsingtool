﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;
using Microsoft.Office.Interop.Excel;
using System.Diagnostics;

namespace AudioRawdataParsingTool
{
    public partial class Form_Main : Form
    {
        ExcelOperation excelOperation, excel_ForChangeTXT;
        string OutputFileName = "", OutputFileFullPath = "";


        public Form_Main()
        {
            InitializeComponent();
        }

        ~Form_Main()
        {
            release_Excel_APP(excelOperation);
        }

        private void button_SelectUPVItemSourceFilePath_Click(object sender, EventArgs e)
        {
            textBox_UPVItemSourceFilePath.Text = selectSourceFoldPath();
        }

        private string selectSourceFoldPath()
        {
            string strFolderPath = "";
            FolderBrowserDialog dialog = new FolderBrowserDialog();
            dialog.Description = "请选择文件夾路径";
            if (dialog.ShowDialog() == DialogResult.OK)
            {
                strFolderPath = dialog.SelectedPath;
            }
            return strFolderPath;
        }

        private void button_SelectFX100SourceFilePath_Click(object sender, EventArgs e)
        {
            textBox_FX100SourceFilePath.Text = selectSourceFoldPath();
        }

        private void button_SelectOuputFilePath_Click(object sender, EventArgs e)
        {
            textBox_SelectOutputFilePath.Text = selectSourceFoldPath();
        }

        private void button_GenerateMergeFile_Click(object sender, EventArgs e)
        {
            if (!is_GenerateConditions_valid())
            {
                return;
            }

            try
            {
                create_OutputFile_FullPath();

                bool isUPVSuccess = handle_UPV_Rawdata();

                bool isFX100Success = handle_FX100_Rawdata();

                release_Excel_APP(excelOperation);

                if (isUPVSuccess || isFX100Success)
                {
                    MessageBox.Show("导出\n" + OutputFileFullPath + "\n成功！");
                }
                else
                {
                    // 導出失敗刪除輸出文件
                    if (File.Exists(OutputFileFullPath))
                    {
                        File.Delete(OutputFileFullPath);
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error Message :" + ex.Message);
            }


        }

        private bool is_GenerateConditions_valid()
        {
            if (textBox_SelectOutputFilePath.Text.Equals(""))
            {
                MessageBox.Show("请先选择输出文件夹！");
                return false;
            }

            if (textBox_UPVItemSourceFilePath.Text.Equals("") & textBox_FX100SourceFilePath.Text.Equals(""))
            {
                MessageBox.Show("请先选择原始数据文件夹！");
                return false;
            }

            OutputFileFullPath = "";

            return true;
        }

        private void create_OutputFile_FullPath()
        {
            OutputFileName = Constant.OUTPUT_FILENAME_PREFIX + "_" + DateTime.Now.ToString("yyyy_MMdd_HHmmss") + Constant.EXCEL_FILE_SUFFIX_XLSX;
            OutputFileFullPath = textBox_SelectOutputFilePath.Text + "\\" + OutputFileName;
            excelOperation = new ExcelOperation();
        }

        private Boolean handle_UPV_Rawdata()
        {
            if (textBox_UPVItemSourceFilePath.Text.Equals(""))
            {
                return false;
            }

            Boolean isSuccess = false;

            // 得到原始數據
            System.Data.DataTable[] AllRawdata = get_UPV_AllRawdatas();

            if (AllRawdata != null)
            {
                if (AllRawdata.Length > 0)
                {
                    // Change DataTable TableName to dt_RLR/SLR/SD/RD
                    List<System.Data.DataTable> AllRawdata_By_Mode = format_DataTable_AllRawdata_By_Mode(AllRawdata);

                    // Get SampleCode Location
                    Dictionary<String, List<int>> dic_SampleCodePosition = generate_SampleCode_Position(textBox_UPVItemSourceFilePath.Text);

                    // Input into excel
                    if (export_AllRawdatas2Excel(AllRawdata_By_Mode, dic_SampleCodePosition, true))
                    {
                        isSuccess = true;
                    }

                }
            }

            return isSuccess;
        }

        private System.Data.DataTable[] get_UPV_AllRawdatas()
        {
            System.Data.DataTable[] dtAllRawdata = null;

            if (!Directory.Exists(textBox_UPVItemSourceFilePath.Text))
            {
                MessageBox.Show("未能找到 UPVItem 原始档案路径！");
                return dtAllRawdata;
            }

            DirectoryInfo rootFolder = new DirectoryInfo(textBox_UPVItemSourceFilePath.Text);//UPV
            DirectoryInfo[] dirRootFolderInfo = rootFolder.GetDirectories();
            DirectoryInfo[] dirBeforeFolderInfo = null, dirAfterFolderInfo = null;
            int dtCount = 0;


            // 获取before和after的子文件个数总数
            for (int i = 0; i < dirRootFolderInfo.Length; i++)
            {
                if (dirRootFolderInfo[i].Name.ToUpper().Equals(Constant.BEFORE))
                {
                    dirBeforeFolderInfo = dirRootFolderInfo[i].GetDirectories();
                    dtCount += dirBeforeFolderInfo.Length;
                }
                else if (dirRootFolderInfo[i].Name.ToUpper().Equals(Constant.AFTER))
                {
                    dirAfterFolderInfo = dirRootFolderInfo[i].GetDirectories();
                    dtCount += dirAfterFolderInfo.Length;
                }
            }


            if (dtCount <= 0)
            {
                MessageBox.Show("未能找到 UPV 下 before 和 after 文件夹！");
                return dtAllRawdata;
            }

            return get_UPV_AllRawdatas_To_DataTableArray(dirBeforeFolderInfo, dirAfterFolderInfo, dtCount);
        }

        private System.Data.DataTable[] get_UPV_AllRawdatas_To_DataTableArray(DirectoryInfo[] dirBeforeFolderInfo, DirectoryInfo[] dirAfterFolderInfo, int Datatable_count)
        {
            System.Data.DataTable[] dtAllRawdata = new System.Data.DataTable[Datatable_count];
            FileInfo[] ExcelFiles_by_SampleCode = null; ;
            string SampleCode_FoldName = "", TableName = "";


            // 遍历 before 子文件夹下的所有样品文件夹,按樣品編號取出原始數據
            if (dirBeforeFolderInfo != null)
            {
                for (int i = 0; i < dirBeforeFolderInfo.Length; i++)
                {
                    SampleCode_FoldName = dirBeforeFolderInfo[i].Name.ToUpper();
                    if (SampleCode_FoldName.StartsWith("WU"))
                    {
                        TableName = "dt_" + Constant.BEFORE + "_" + SampleCode_FoldName;

                        // 获取样品文件夹下的所有原始数据档
                        ExcelFiles_by_SampleCode = dirBeforeFolderInfo[i].GetFiles();

                        if (ExcelFiles_by_SampleCode.Length > 0)
                        {
                            dtAllRawdata[i] = get_UPV_Rawdata_by_SampleCode(TableName, ExcelFiles_by_SampleCode);
                        }
                    }
                    else
                    {
                        dtAllRawdata[i] = new System.Data.DataTable("dt_NoRawdata");
                    }
                }
            }


            // 遍历 after 子文件夹下的所有样品文件夹,按樣品編號取出原始數據
            int index_Head = 0;
            if (dirBeforeFolderInfo != null)
            {
                index_Head = dirBeforeFolderInfo.Length;
            }

            for (int i = index_Head; i < Datatable_count; i++)
            {
                SampleCode_FoldName = dirAfterFolderInfo[i - index_Head].Name.ToUpper();
                if (SampleCode_FoldName.StartsWith("WU"))
                {
                    TableName = "dt_" + Constant.AFTER + "_" + SampleCode_FoldName;

                    // 获取样品文件夹下的所有原始数据档
                    ExcelFiles_by_SampleCode = dirAfterFolderInfo[i - index_Head].GetFiles();

                    if (ExcelFiles_by_SampleCode.Length > 0)
                    {
                        dtAllRawdata[i] = get_UPV_Rawdata_by_SampleCode(TableName, ExcelFiles_by_SampleCode);
                    }
                }
                else
                {
                    dtAllRawdata[i] = new System.Data.DataTable("dt_NoRawdata");
                }
            }

            return dtAllRawdata;
        }

        private System.Data.DataTable get_UPV_Rawdata_by_SampleCode(string dtName, FileInfo[] SampleFileInfo)
        {
            System.Data.DataTable dt_Rawdata = new System.Data.DataTable(dtName);
            for (int i = 0; i < SampleFileInfo.Length; i++)
            {
                get_Rawdatas_to_DataTable_by_TestMode(dt_Rawdata, SampleFileInfo[i]);
            }

            return dt_Rawdata;
        }

        private void get_Rawdatas_to_DataTable_by_TestMode(System.Data.DataTable dt, FileInfo RawdataFile)
        {
            if (RawdataFile.Exists)
            {
                string FileName = RawdataFile.Name;
                switch (FileName)
                {
                    case Constant.WORKBOOK_NAME_RLR_WITH_SUFFIX:
                        bind_AllRawdatas_DataTable(dt, RawdataFile.FullName, Constant.WORKBOOK_NAME_RLR, Constant.KEY_WORDS_TO_FIND_RAWDATA_RLR);
                        break;

                    case Constant.WORKBOOK_NAME_SLR_WITH_SUFFIX:
                        bind_AllRawdatas_DataTable(dt, RawdataFile.FullName, Constant.WORKBOOK_NAME_SLR, Constant.KEY_WORDS_TO_FIND_RAWDATA_SLR);
                        break;

                    case Constant.WORKBOOK_NAME_RD_WITH_SUFFIX:
                        bind_AllRawdatas_DataTable(dt, RawdataFile.FullName, Constant.WORKBOOK_NAME_RD, Constant.KEY_WORDS_TO_FIND_RAWDATA_RD);
                        break;

                    case Constant.WORKBOOK_NAME_SD_WITH_SUFFIX:
                        bind_AllRawdatas_DataTable(dt, RawdataFile.FullName, Constant.WORKBOOK_NAME_SD, Constant.KEY_WORDS_TO_FIND_RAWDATA_SD);
                        break;

                    default:
                        // Handle FX100 DATA
                        if (FileName.ToUpper().StartsWith("WU"))
                        {
                            excelOperation.get_FX100_Rawdata_By_Workbook(dt, RawdataFile.FullName);
                        }
                        break;
                }
            }
        }

        private void bind_AllRawdatas_DataTable(System.Data.DataTable dt, string Workbook_fullfilename,
                                                string Workbook_name, string Key_Text_To_Find_Rawdata)
        {
            List<string> lst_rawdata = excelOperation.get_UPV_Rawdata_By_KeyWords(Workbook_fullfilename, Key_Text_To_Find_Rawdata);
            Utils.BindDataTable(dt, lst_rawdata, Workbook_name);
        }

        private List<System.Data.DataTable> format_DataTable_AllRawdata_By_Mode(System.Data.DataTable[] dtAllRawdata)
        {
            List<System.Data.DataTable> dtAllRawdataByMode = new List<System.Data.DataTable>();
            System.Data.DataTable dt_Origin, dt_New;
            string dt_OriginTableName = "", dt_NewTableName = "";
            string Origin_CollumnName = "", New_CollumnName = "";

            for (int i = 0; i < dtAllRawdata.Length; i++)
            {
                dt_Origin = dtAllRawdata[i];
                dt_OriginTableName = dt_Origin.TableName;

                if (!dt_OriginTableName.Equals("dt_NoRawdata"))
                {
                    string[] Origin_TableName = dt_OriginTableName.Split('_');
                    if (Origin_TableName.Length == 3)
                    {
                        for (int j = 0; j < dt_Origin.Columns.Count; j++)
                        {
                            Origin_CollumnName = dt_Origin.Columns[j].ColumnName;

                            dt_NewTableName = "dt_" + Origin_CollumnName;

                            New_CollumnName = dt_OriginTableName.Substring(3, dt_OriginTableName.Length - 3);

                            if (!isDataTableArrayContainsTable(dtAllRawdataByMode, dt_NewTableName))
                            {
                                dt_New = new System.Data.DataTable(dt_NewTableName);

                                add_Collumn_to_NewDataTable(dt_Origin, dt_New, Origin_CollumnName, New_CollumnName);

                                dtAllRawdataByMode.Add(dt_New);
                            }
                            else
                            {
                                add_Collumn_to_ExistedDataTable(dt_Origin, dtAllRawdataByMode, dt_NewTableName, Origin_CollumnName, New_CollumnName);
                            }
                        }
                    }
                }
            }

            return dtAllRawdataByMode;
        }

        private Dictionary<string, List<int>> generate_SampleCode_Position(string Rawdata_Root_Folder)
        {
            Dictionary<String, List<int>> dic_SampleCodePosition = get_FolderName_By_Path(Rawdata_Root_Folder);
            int i = 0;
            foreach (KeyValuePair<string, List<int>> item in dic_SampleCodePosition)
            {
                int Sample_Location_Row = 4;
                int Sample_Location_Column = 2 + 7 * i;
                dic_SampleCodePosition[item.Key].Add(Sample_Location_Row);
                dic_SampleCodePosition[item.Key].Add(Sample_Location_Column);
                i++;
            }
            return dic_SampleCodePosition;
        }

        private Dictionary<string, List<int>> get_FolderName_By_Path(string root_folder)
        {
            Dictionary<string, List<int>> dic_return = new Dictionary<string, List<int>>();
            if (Directory.Exists(root_folder))
            {
                DirectoryInfo rootFolder = new DirectoryInfo(root_folder);// UPV
                DirectoryInfo[] dirRootFolderInfo = rootFolder.GetDirectories();
                DirectoryInfo[] dirBeforeFolderInfo = null, dirAfterFolderInfo = null;
                Dictionary<String, List<int>> dic_TempSampleCodePosition = new Dictionary<string, List<int>>();


                // 获取before和after下的樣品編號文件夾名稱
                for (int i = 0; i < dirRootFolderInfo.Length; i++)
                {
                    if (dirRootFolderInfo[i].Name.ToUpper().Equals(Constant.BEFORE))
                    {
                        dirBeforeFolderInfo = dirRootFolderInfo[i].GetDirectories();
                    }
                    else if (dirRootFolderInfo[i].Name.ToUpper().Equals(Constant.AFTER))
                    {
                        dirAfterFolderInfo = dirRootFolderInfo[i].GetDirectories();
                    }
                }

                for (int i = 0; i < dirBeforeFolderInfo.Length; i++)
                {
                    dic_TempSampleCodePosition.Add(dirBeforeFolderInfo[i].Name, new List<int>());
                }

                for (int i = 0; i < dirAfterFolderInfo.Length; i++)
                {
                    if (!dic_TempSampleCodePosition.ContainsKey(dirAfterFolderInfo[i].Name))
                    {
                        dic_TempSampleCodePosition.Add(dirAfterFolderInfo[i].Name, new List<int>());
                    }
                }

                // 按樣品編號排序
                dic_return = dic_TempSampleCodePosition.OrderBy(key => key.Key).ToDictionary((keyItem) => keyItem.Key, (valueItem) => valueItem.Value);

            }

            return dic_return;
        }

        private bool export_AllRawdatas2Excel(List<System.Data.DataTable> dtAllRawdata,
          Dictionary<string, List<int>> dic_SampleCodePosition, bool need_copy_template_file)
        {
            return excelOperation.export_DataTable_To_Excel(OutputFileFullPath, dtAllRawdata, dic_SampleCodePosition, need_copy_template_file);
        }

        private Boolean handle_FX100_Rawdata()
        {
            if (textBox_FX100SourceFilePath.Text.Equals(""))
            {
                return false;
            }

            Boolean isSuccess = false;

            // Copy txt to excel and save to TempExcelFile
            if (!change_TXT2Excel())
            {
                return false;
            }

            // Get All Rawdatas into DataTable[]
            System.Data.DataTable[] AllRawdata = get_FX100_AllRawdatas();

            if (AllRawdata != null)
            {
                if (AllRawdata.Length > 0)
                {
                    // Change DataTable TableName to dt_SpeakerFR/SpeakerTHD/SpeakerRB
                    List<System.Data.DataTable> AllRawdata_By_Mode = format_DataTable_AllRawdata_By_Mode(AllRawdata);

                    // Get SampleCode Position
                    Dictionary<string, List<int>> dic_SampleCodePosition = generate_SampleCode_Position(textBox_FX100SourceFilePath.Text);

                    // Export into excel
                    bool Need_Copy_template_file = need_Copy_template_file();
                    if (export_AllRawdatas2Excel(AllRawdata_By_Mode, dic_SampleCodePosition, Need_Copy_template_file))
                    {
                        isSuccess = true;
                    }

                    release_Excel_APP(excel_ForChangeTXT);

                    // 刪除臨時文件(TXT轉Excel)
                    delete_temp_files();
                }
            }


            return isSuccess;
        }

        private Boolean change_TXT2Excel()
        {
            Boolean isChangeSuccess = false;
            string strRootFolder = textBox_FX100SourceFilePath.Text;

            if (Directory.Exists(textBox_FX100SourceFilePath.Text))
            {
                DirectoryInfo rootFolder = new DirectoryInfo(textBox_FX100SourceFilePath.Text);//FX100
                DirectoryInfo[] dirRootFolderInfo = rootFolder.GetDirectories();//before、after
                List<string[]> lst_array_TempExcelName = new List<string[]>();

                // Create Temp Document
                lst_array_TempExcelName = create_Temp_Excelfile(dirRootFolderInfo, strRootFolder);

                if (lst_array_TempExcelName.Count > 0)
                {
                    // Parsing TXT to Excel
                    parsing_TXT_Files(dirRootFolderInfo, strRootFolder);

                    isChangeSuccess = true;
                }
                else
                {
                    delete_temp_files();
                    MessageBox.Show("未能找到 FX100 下 before 和 after 文件夹！");
                }
            }
            else
            {
                MessageBox.Show("未能找到 FX100 原始档案路径！");
            }

            return isChangeSuccess;
        }

        private List<string[]> create_Temp_Excelfile(DirectoryInfo[] dirRootFolderInfo, string rootFolder)
        {
            List<string[]> listarray_TempExcelName = new List<string[]>();
            DirectoryInfo[] dirSampleFolderInfo = null;
            string tempFolderPath = "", subSampleFoldName = "", tempFolderBefore = "", tempFolderAfter = "";
            string[] SampleCodeList = null;

            if (dirRootFolderInfo != null)
            {
                if (dirRootFolderInfo.Length > 0)
                {
                    // Create TempExcelFile Folder  
                    tempFolderBefore = rootFolder + "\\" + Constant.TEMP_FOLDER_BEFORE;
                    tempFolderAfter = rootFolder + "\\" + Constant.TEMP_FOLDER_AFTER;
                    if (!Directory.Exists(tempFolderBefore))
                    {
                        Directory.CreateDirectory(tempFolderBefore);
                    }

                    if (!Directory.Exists(tempFolderAfter))
                    {
                        Directory.CreateDirectory(tempFolderAfter);
                    }

                    // Get Temp Excel File Name and then create Excel File named with SampleCode
                    for (int i = 0; i < dirRootFolderInfo.Length; i++)
                    {
                        tempFolderPath = "";
                        dirSampleFolderInfo = dirRootFolderInfo[i].GetDirectories();//before/after
                        SampleCodeList = new string[dirSampleFolderInfo.Length];

                        if (dirRootFolderInfo[i].ToString().ToUpper().Equals(Constant.BEFORE))
                        {
                            tempFolderPath = tempFolderBefore;
                        }
                        else if (dirRootFolderInfo[i].ToString().ToUpper().Equals(Constant.AFTER))
                        {
                            tempFolderPath = tempFolderAfter;
                        }

                        if (!tempFolderPath.Equals(""))
                        {
                            for (int j = 0; j < dirSampleFolderInfo.Length; j++)
                            {
                                subSampleFoldName = dirSampleFolderInfo[j].Name.ToUpper();//WU***
                                if (subSampleFoldName.StartsWith("WU"))
                                {
                                    SampleCodeList[j] = subSampleFoldName;
                                }
                            }

                            listarray_TempExcelName.Add(SampleCodeList);

                            excel_ForChangeTXT = new ExcelOperation();
                            excel_ForChangeTXT.batch_Create_Excel(listarray_TempExcelName[listarray_TempExcelName.Count - 1], tempFolderPath);
                        }
                    }
                }
            }

            return listarray_TempExcelName;
        }

        private void parsing_TXT_Files(DirectoryInfo[] dirRootFolderInfo, string rootFolder)
        {
            DirectoryInfo[] dirRawdataFolderInfo = null;
            FileInfo[] subTxtFileInfo = null;
            string tempFolderPath = "", subSampleFoldName = "";
            string tempFolderBefore = "", tempFolderAfter = "";
            string tempExcelFilePath = "";

            tempFolderBefore = rootFolder + "\\" + Constant.TEMP_FOLDER_BEFORE;
            tempFolderAfter = rootFolder + "\\" + Constant.TEMP_FOLDER_AFTER;
            //Parsing TXT to Excel
            for (int i = 0; i < dirRootFolderInfo.Length; i++)
            {
                tempFolderPath = "";
                if (dirRootFolderInfo[i].ToString().ToUpper().Equals(Constant.BEFORE))
                {
                    tempFolderPath = tempFolderBefore;
                }
                else if (dirRootFolderInfo[i].ToString().ToUpper().Equals(Constant.AFTER))
                {
                    tempFolderPath = tempFolderAfter;
                }

                if (!tempFolderPath.Equals(""))
                {
                    dirRawdataFolderInfo = dirRootFolderInfo[i].GetDirectories();//before/after
                    for (int j = 0; j < dirRawdataFolderInfo.Length; j++)
                    {
                        subSampleFoldName = dirRawdataFolderInfo[j].Name.ToUpper();
                        if (subSampleFoldName.StartsWith("WU"))
                        {
                            subTxtFileInfo = dirRawdataFolderInfo[j].GetFiles();
                            if (subTxtFileInfo.Length > 0)
                            {
                                tempExcelFilePath = tempFolderPath + "\\" + subSampleFoldName + Constant.EXCEL_FILE_SUFFIX_XLS;
                                excel_ForChangeTXT.parsing_TXT2Excel(subTxtFileInfo, tempFolderPath, tempExcelFilePath);
                            }
                        }
                    }

                    if (File.Exists(tempFolderPath + Constant.TEMP_EXCELFILE))
                    {
                        File.Delete(tempFolderPath + Constant.TEMP_EXCELFILE);
                    }
                }
            }
        }

        private void add_Collumn_to_NewDataTable(System.Data.DataTable dt_Origin, System.Data.DataTable dt_New, string OriginCollumnName, string NewCollumnName)
        {
            if (dt_Origin != null && dt_New != null)
            {
                dt_New.Columns.Add(NewCollumnName, System.Type.GetType("System.String"));
                int Columnindex = dt_Origin.Columns.IndexOf(OriginCollumnName);
                DataRow NewRow;

                for (int i = 0; i < dt_Origin.Rows.Count; i++)
                {
                    string Row_Text = dt_Origin.Rows[i].ItemArray[Columnindex].ToString();
                    if (!Row_Text.Equals(""))
                    {
                        NewRow = dt_New.NewRow();
                        NewRow[NewCollumnName] = Row_Text;
                        dt_New.Rows.Add(NewRow);
                    }

                }
            }
        }

        private void add_Collumn_to_ExistedDataTable(System.Data.DataTable dt_Origin, List<System.Data.DataTable> listTable, string TableName, string OriginCollumnName, string NewCollumnName)
        {
            if (dt_Origin != null && listTable != null)
            {
                DataRow NewRow;
                System.Data.DataTable dt_New = null;
                int Columnindex = dt_Origin.Columns.IndexOf(OriginCollumnName);

                for (int i = 0; i < listTable.Count; i++)
                {
                    if (listTable[i].TableName.Equals(TableName))
                    {
                        dt_New = listTable[i];
                        dt_New.Columns.Add(NewCollumnName, System.Type.GetType("System.String"));
                        break;
                    }
                }

                for (int i = 0; i < dt_Origin.Rows.Count; i++)
                {
                    string Row_Text = dt_Origin.Rows[i].ItemArray[Columnindex].ToString();
                    if (!Row_Text.Equals(""))
                    {
                        NewRow = dt_New.NewRow();
                        NewRow[NewCollumnName] = Row_Text;
                        dt_New.Rows.Add(NewRow);
                    }
                }

            }
        }

        private Boolean isDataTableArrayContainsTable(List<System.Data.DataTable> dt, string tableName)
        {
            Boolean isContains = false;
            if (dt != null)
            {
                for (int i = 0; i < dt.Count; i++)
                {
                    if (dt[i].TableName.Equals(tableName))
                    {
                        isContains = true;
                    }
                }
            }
            return isContains;
        }

        private System.Data.DataTable[] get_FX100_AllRawdatas()
        {
            System.Data.DataTable[] dtAllRawdata = null;
            if (!Directory.Exists(textBox_FX100SourceFilePath.Text))
            {
                MessageBox.Show("未能找到 FX100 原始档案路径！");
                return dtAllRawdata;
            }

            DirectoryInfo rootFolder = new DirectoryInfo(textBox_FX100SourceFilePath.Text);//FX100
            DirectoryInfo[] dirRootFolderInfo = rootFolder.GetDirectories();
            FileInfo[] subBeforeTempExcelFileInfo = null, subAfterTempExcelFileInfo = null;
            int dtCount = 0;


            // 获取 beforeTempExcelFile 和 afterTempExcelFile 的子文件个数总数
            for (int i = 0; i < dirRootFolderInfo.Length; i++)
            {
                if (dirRootFolderInfo[i].Name.Equals(Constant.TEMP_FOLDER_BEFORE))
                {
                    subBeforeTempExcelFileInfo = dirRootFolderInfo[i].GetFiles();
                    dtCount += subBeforeTempExcelFileInfo.Length;
                }
                else if (dirRootFolderInfo[i].Name.Equals(Constant.TEMP_FOLDER_AFTER))
                {
                    subAfterTempExcelFileInfo = dirRootFolderInfo[i].GetFiles();
                    dtCount += subAfterTempExcelFileInfo.Length;
                }
            }


            if (dtCount <= 0)
            {
                MessageBox.Show("未能找到FX100下before和after文件夹！");
                return dtAllRawdata;
            }

            return get_FX100_AllRawdatas_To_DataTableArray(subBeforeTempExcelFileInfo, subAfterTempExcelFileInfo, dtCount);
        }

        private System.Data.DataTable[] get_FX100_AllRawdatas_To_DataTableArray(FileInfo[] subBeforeTempExcelFileInfo, FileInfo[] subAfterTempExcelFileInfo, int Datatable_count)
        {
            System.Data.DataTable[] dtAllRawdata = new System.Data.DataTable[Datatable_count];
            string subSampleCodeExcelFileName = "", SampleCode = "", TableName = "";

            // 遍历 beforeTempExcelFile 子文件夹下的所有原始数据档
            for (int i = 0; i < subBeforeTempExcelFileInfo.Length; i++)
            {
                subSampleCodeExcelFileName = subBeforeTempExcelFileInfo[i].Name.ToUpper();
                if (subSampleCodeExcelFileName.StartsWith("WU"))
                {
                    SampleCode = subSampleCodeExcelFileName.Split('.')[0];
                    TableName = "dt_" + Constant.TEMP_FOLDER_BEFORE + "_" + SampleCode;
                    dtAllRawdata[i] = get_FX100_Rawdata_By_SampleCode(TableName, subBeforeTempExcelFileInfo[i]);
                }
                else
                {
                    dtAllRawdata[i] = new System.Data.DataTable("dt_NoRawdata");
                }
            }


            // 遍历 afterTempExcelFile 子文件夹下的所有原始数据档
            for (int i = subBeforeTempExcelFileInfo.Length; i < Datatable_count; i++)
            {
                subSampleCodeExcelFileName = subAfterTempExcelFileInfo[i - subBeforeTempExcelFileInfo.Length].Name.ToUpper();
                if (subSampleCodeExcelFileName.StartsWith("WU"))
                {
                    SampleCode = subSampleCodeExcelFileName.Split('.')[0];
                    TableName = "dt_" + Constant.TEMP_FOLDER_AFTER + "_" + SampleCode;
                    dtAllRawdata[i] = get_FX100_Rawdata_By_SampleCode(TableName, subAfterTempExcelFileInfo[i - subBeforeTempExcelFileInfo.Length]);
                }
                else
                {
                    dtAllRawdata[i] = new System.Data.DataTable("dt_NoRawdata");
                }
            }

            return dtAllRawdata;
        }

        private System.Data.DataTable get_FX100_Rawdata_By_SampleCode(string dtName, FileInfo SampleFileInfo)
        {
            System.Data.DataTable dt_Rawdata = new System.Data.DataTable(dtName);
            get_Rawdatas_to_DataTable_by_TestMode(dt_Rawdata, SampleFileInfo);

            return dt_Rawdata;
        }

        private bool need_Copy_template_file()
        {
            bool need_Copy_template_file = true;
            if (textBox_UPVItemSourceFilePath.Text != "")
            {
                need_Copy_template_file = false;
            }

            return need_Copy_template_file;
        }

        private void delete_temp_files()
        {
            if (Directory.Exists(textBox_FX100SourceFilePath.Text + "\\" + Constant.TEMP_FOLDER_BEFORE))
            {
                Directory.Delete(textBox_FX100SourceFilePath.Text + "\\" + Constant.TEMP_FOLDER_BEFORE, true);
            }
            if (Directory.Exists(textBox_FX100SourceFilePath.Text + "\\" + Constant.TEMP_FOLDER_AFTER))
            {
                Directory.Delete(textBox_FX100SourceFilePath.Text + "\\" + Constant.TEMP_FOLDER_AFTER, true);
            }
        }

        private void release_Excel_APP(ExcelOperation excelOperation)
        {
            if (excelOperation != null)
            {
                excelOperation.quitExcelApp();
                excelOperation = null;
                GC.Collect();
            }
        }

        private void 帮助ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MessageBox.Show("1. 原始数据档案夹路径下应包括 before/after 文件夹，在这两个文件夹下应包括以样品编号命名(WU 開頭)的文件夹，"
                + "样品编号文件夹下应包含 RD/SD/RLR/SLR 的 Excel 档案或 TXT 档案.\n"
                + "2. Template.xlsx 模板文件应与本程式同一目录！");
        }
    }
}
