﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;



namespace AudioRawdataParsingTool
{
    public class Constant
    {
        public const string TEMPLATE_FILENAME = "Template.xlsx";
        public const string OUTPUT_FILENAME_PREFIX = "OutputFile";
        public const string ROOT_FOLDER_NAME_UPV = "UPV";//FolderName不区分大小写
        public const string ROOT_FOLDER_NAME_FX100 = "FX100";//FolderName不区分大小写
        public const string BEFORE = "BEFORE";
        public const string AFTER = "AFTER";
        public const string TEMP_FOLDER_BEFORE = "beforeTempExcelFile";
        public const string TEMP_FOLDER_AFTER = "afterTempExcelFile";
        public const string TXT_NAME_PASSEDFAILED = "PassedFailed";

        public const string EXCEL_FILE_SUFFIX_XLSX = ".xlsx";
        public const string EXCEL_FILE_SUFFIX_XLS = ".xls";
        public const string TEMP_EXCELFILE="\\temp.xls";

        public const string WORKBOOK_NAME_RLR = "RLR";
        public const string WORKBOOK_NAME_SLR = "SLR";
        public const string WORKBOOK_NAME_RD = "RD";
        public const string WORKBOOK_NAME_SD = "SD";
        public const string WORKBOOK_NAME_RLR_WITH_SUFFIX = WORKBOOK_NAME_RLR + EXCEL_FILE_SUFFIX_XLS;
        public const string WORKBOOK_NAME_SLR_WITH_SUFFIX = WORKBOOK_NAME_SLR + EXCEL_FILE_SUFFIX_XLS;
        public const string WORKBOOK_NAME_RD_WITH_SUFFIX = WORKBOOK_NAME_RD + EXCEL_FILE_SUFFIX_XLS;
        public const string WORKBOOK_NAME_SD_WITH_SUFFIX = WORKBOOK_NAME_SD + EXCEL_FILE_SUFFIX_XLS;

        //UPV Sheet Name
        public const string TEMPLATE_SHEET_SLR = "Microphone FR";
        public const string TEMPLATE_SHEET_SD = "Microphone distortion";
        public const string TEMPLATE_SHEET_RLR = "Receiver FR";
        public const string TEMPLATE_SHEET_RD = "Receiver distortion";

        //FX100 Sheet Name
        public const string TEMPLATE_SHEET_FR = "Speaker FR";
        public const string TEMPLATE_SHEET_THD = "Speaker THD";
        public const string TEMPLATE_SHEET_RB = "Speaker R&B";


        public const string KEY_WORDS_TO_FIND_RAWDATA_RLR = "Receiving sensitivity";
        public const string KEY_WORDS_TO_FIND_RAWDATA_SLR = "Sending sensitivity";
        public const string KEY_WORDS_TO_FIND_RAWDATA_RD = "Measured";
        public const string KEY_WORDS_TO_FIND_RAWDATA_SD = "Measured";


        public const int RAWDATA_COLLUMN = 3;


    }



}
